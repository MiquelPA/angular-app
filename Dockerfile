FROM node:15.11.0 as build-step

# Create Directory for the Container
WORKDIR /app
# Only copy the package.json file to work directory
COPY package.json .
# Install all Packages
RUN npm install
# Copy all other source code to work directory
ADD . /app
# TypeScript
RUN npm run build

# Stage 2
FROM nginx:1.17.1-alpine
COPY --from=build-step /app/dist/angular-app /usr/share/nginx/html
