import {Component, OnInit} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {Store, Select} from '@ngxs/store';
import {NewEntityList} from '../redux/actions/entity.action';
import {Login} from '../redux/actions/auth.action';
import {State} from '../types/types';
import {Entity} from '../types/exchange';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    @Select((state: State) => state.entities?.Sensor?.lists?.sensors?.items || []) _sensors$: Observable<string[]>;
    @Select((state: State) => state.entities?.SensorType?.lists?.sensorTypes?.items || []) _sensorTypes$: Observable<string[]>;
    @Select((state: State) => state.entities?.SensorReading?.lists?.sensorReadings?.items || []) _sensorReadings$: Observable<string[]>;

    public sensor$: Observable<any>;

    constructor(private store: Store) {

        this.store.dispatch(new Login('miquel.piza.airas@gmail.com', '12345678'));

        this.store.dispatch(new NewEntityList('Sensor', 'sensors'));
        this.store.dispatch(new NewEntityList('SensorType', 'sensorTypes'));
        this.store.dispatch(new NewEntityList('SensorReading', 'sensorReadings', {}, {
            keys: ['timestamp'],
            directions: [-1],
            directionsStr: ['desc']
        }));

        this.sensor$ = combineLatest([
            this._sensors$,
            this._sensorTypes$,
            this._sensorReadings$,
        ]).pipe(
            map(([sensors, sensorTypes, sensorReadings]: [any[], any[], any[]]) => {
                return sensors.map(sensor => {
                    const type = sensorTypes.find(st => st.id === sensor.type);
                    if (!type) {
                        return null;
                    }
                    const readings = sensorReadings
                        .filter(sr => sr.sensorId === sensor.id)
                        .map(sr => ({...sr, date: new Date(sr.timestamp).toDateString()}));
                    return {
                        location: sensor.location,
                        type: type.name,
                        minValue: type.minValue,
                        maxValue: type.maxValue,
                        readings,
                    };
                }).filter(s => !!s);
            })
        );
    }

    ngOnInit(): void {
    }
}
