import * as _ from 'lodash';
import {ListEntityOptions, Entity, Sort, EntityFilters} from '../types/exchange';
// TODO: merge this types and types.ts
/*
export type EntitySort = {
    [key: string]: 'asc' | 'desc',
};*/

export interface EntitySingleDefinition {
    modelName: string;
    id: string;
}
export class EntitySingle {
    public data: Entity;
    public loading = true;
    public readonly def: EntitySingleDefinition;
    constructor(id: string, modelName: string) {
        this.def = {
            id,
            modelName,
        };
    }
}
export interface EntityListDefinition extends ListEntityOptions {
    name: string;
}
export class EntityList {
    public items: Entity[] = [];
    public loading = true;
    public readonly def: EntityListDefinition;
    constructor(name: string, options: ListEntityOptions) {
        this.def =  {
            name,
            ...options
        };
    }
}
// Must be same model type
function match(list: EntityList, entity: Entity): boolean {
    return true; // TODO: TODO
}
export function contains(list: EntityList, entity: Entity): boolean {
    return !!list.items.find((item) => item.id === entity.id);
}

export function belongs(list: EntityList, entity: Entity): boolean {
    return position(list, entity) < list.items.length && list.def?.filters ? match(list, entity) : true;
}

/**
 * Return new list with element inserted and sorted
 * @param list
 */
export function combine(list: EntityList, entity: Entity): EntityList {
    // Find if it is already present
    const idx = list.items.findIndex((item) => item.id === entity.id);
    const items = [...list.items];
    // Remove and replace if found, otherwise add
    idx > -1 ? items.splice(idx, 1, {...entity}) : items.push(entity);
    // Sort and return
    return {
        ...list,
        items: _.orderBy(items, list.def.sort.keys, list.def.sort.directionsStr)
    };
}

// TODO: test
export function position(list: EntityList, entity: Entity): number {
    let pos = list.items.length;
    const len = pos;
    let i = 0;
    while (pos >= len && i < len) {
        if (compare<Entity>(entity, list.items[i], list.def.sort) < 0) {
            pos = i;
        }
        i++;
    }
    return pos;
}

/**
 * -1 if a comes before b
 * @param a
 * @param b
 * @param sort
 */
export function compare<T extends Entity>(a: T, b: T, sort: Sort): -1 | 0 | 1 {
    let order = 0;
    let i = 0;
    while (order === 0 && i < sort.keys.length) {
        order = typeof a[sort.keys[i]] === 'number' ?
            (a[sort.keys[i]] < b[sort.keys[i]] ? -1 : 1) * sort.directions[i]
            : typeof a[sort.keys[i]] === 'string' ?
                a[sort.keys[i]].localeCompare(b[sort.keys[i]]) * sort.directions[i]
                : 0;
        i++;
    }
    return order as (-1 | 0 | 1);
}
