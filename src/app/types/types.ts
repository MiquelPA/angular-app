import {EntityStateModel} from '../redux/states/entity.state';
import {AuthStateModel} from '../redux/states/auth.state';

export type State = {
    entities: EntityStateModel,
    auth: AuthStateModel,
};
