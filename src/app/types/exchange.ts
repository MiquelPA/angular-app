// TODO: export from server project?
export type Sort = {
    keys: string[],
    directions: (1 | -1)[],
    directionsStr: ('asc' | 'desc')[],
};
export const defaultSort: Sort = {
    keys: ['_id'],
    directions: [-1],
    directionsStr: ['desc'],
};
export type PermissionCode = number;
export type Permissions = {
    [userId: string]: PermissionCode
};
export interface EntityBaseContent {
    createdBy: string;
    updatedBy: string;
    createdAt: Date;
    updatedAt: Date;
    permissions: Permissions;
}
export interface EntityContent {
    [prop: string]: any,
}
export interface Entity extends EntityContent, EntityBaseContent{
    id?: string;
}
export type ResponseStatus = 'success' | 'error';

export interface ResponsePayload {
    status: ResponseStatus;
    data?: any;
    error?: string;
    errors?: string[];
    returnData?: ReturnData;
}

export type EntityFilters = {
    [k: string]: any
};
export interface BaseEntityOptions {
    type: 'get' | 'delete' | 'list' | 'create' | 'update';
    modelName: string;
}
export class GetEntityOptions implements BaseEntityOptions {
    constructor(public modelName: string, public id: string, public view?: string, public type: 'get' = 'get') {}
}
export class CreateEntityOptions implements BaseEntityOptions {
    constructor(public modelName: string, public data: EntityContent, public type: 'create' = 'create') {}
}
export class ListEntityOptions implements BaseEntityOptions {
    constructor(public modelName: string, public filters: EntityFilters = {}, public sort: Sort = defaultSort,
                public count: number = 0, public offset: number = 0, public view?: string, public type: 'list' = 'list') {}
}
export class UpdateEntityOptions implements BaseEntityOptions {
    constructor(public modelName: string, public id: string, public data: EntityContent, public type: 'update' = 'update') {}
}
export class DeleteEntityOptions implements BaseEntityOptions {
    constructor(public modelName: string, public id: string, public type: 'delete' = 'delete') {}
}
/*export class EntityOptions implements BaseEntityOptions {
    constructor(public modelName: string, public type: null = null) {}
}*/

export interface RemoteActionDefinition {
    type?: string; // Is static in action class so ts doesn't register it as a required property and mismatches with this
    data: any;
    [arg: string]: any;
}
export interface ReturnData {
    action?: {
        success?: RemoteActionDefinition,
        error?: RemoteActionDefinition,
    };
    [k: string]: any;
}
export class RequestPayload<EntityOptionsType = undefined, DataType = any> {
    constructor(
        public data?: DataType,
        public entityOptions?: EntityOptionsType,
        public returnData?: ReturnData,
        public broadcastResponse: boolean = false,
    ) {
    }
}
export interface WebSocketRequestPayload<EntityOptionsType = undefined, DataType = any> extends RequestPayload<EntityOptionsType, DataType> {
    endpoint?: string;
}

export const PERMISSIONS: {[id: string]: PermissionCode} = {
    NONE:               0b0000000000000001, // 1
    VIEW:               0b0000000000000010, // 2
    EDIT:               0b0000000000000100, // 4
    CREATE:             0b0000000000001000, // 8
    DELETE:             0b0000000000010000, // 16
    CHANGE_PERMISSIONS: 0b0000000000100000, // 32
};
export const ROLES: {[id: string]: PermissionCode} = {
    NONE:   0b0000000000000001,
    GUEST:  PERMISSIONS.VIEW,
    USER:   PERMISSIONS.VIEW | PERMISSIONS.EDIT | PERMISSIONS.CREATE,
    ADMIN:  PERMISSIONS.VIEW | PERMISSIONS.EDIT | PERMISSIONS.CREATE | PERMISSIONS.DELETE | PERMISSIONS.CHANGE_PERMISSIONS,
    OWNER:  PERMISSIONS.VIEW | PERMISSIONS.EDIT | PERMISSIONS.CREATE | PERMISSIONS.DELETE | PERMISSIONS.CHANGE_PERMISSIONS,
};
export type AuthCredentials = {
    email: string,
    password: string,
};
