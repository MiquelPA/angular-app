import {Entity, ListEntityOptions, Sort, EntityFilters, RemoteActionDefinition} from '../../types/exchange';

export class AddEntity {
  static readonly type = '[Entity] Add';
  constructor(public payload: Entity) {
  }
}
export class UpdateEntity {
  static readonly type = '[Entity] Update';
  constructor(public payload: Entity, public id: number) {
  }
}

export class DeleteEntity {
  static readonly type = '[Entity] Delete';
  constructor(public id: number) {
  }
}

export class NewEntityList {
    static readonly type = '[Entity] NewList';
    constructor(public modelName: string, public listName: string, public filters?: EntityFilters, public sort?: Sort, public count?: number) {
    }
}
export class NewEntitySingle {
    static readonly type = '[Entity] NewSingle';
    constructor(public modelName: string, public id: string) {
    }
}

export class SetEntitySingle implements RemoteActionDefinition {
  static readonly type = '[Entity] SetSingle';
  constructor(public modelName: string, public data: Entity) {
  }
}
export class SetEntityList implements RemoteActionDefinition {
  static readonly type = '[Entity] SetList';
  constructor(public listName: string, public listOptions: ListEntityOptions, public offset = 0, public data: Entity[] = []) {
  }
}
