export class Login {
  static readonly type = '[Auth] Login';

  constructor(public email: string, public password: string) {
  }
}
export class LoggedIn {
  static readonly type = '[Auth] LoggedIn';

  constructor(public userData: any) {
  }
}
