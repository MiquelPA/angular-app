import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {LoggedIn, Login} from '../actions/auth.action';

export class AuthStateModel {
    user: any; // TODO: import UserData type from server
}

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        user: null,
    }
})
@Injectable()
export class AuthState {
    constructor(private authService: AuthService) {
    }

    /**
     * @param ctx
     * @param action
     */
    @Action(Login)
    login(ctx: StateContext<AuthStateModel>, action: Login): void {
        this.authService.login(action.email, action.password);
    }
    /**
     * @param ctx
     * @param action
     */
    @Action(LoggedIn)
    loggedIn(ctx: StateContext<AuthStateModel>, action: LoggedIn): void {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            user: {...action.userData}
        });
    }

}
