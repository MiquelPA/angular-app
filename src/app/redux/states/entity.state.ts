import {belongs, combine, contains, EntityList, EntitySingle} from '../../models/entity';
import {Action, State, StateContext} from '@ngxs/store';
import {EntityService} from '../../services/entity/entity.service';
import {Injectable} from '@angular/core';
import {NewEntityList, SetEntitySingle, SetEntityList, NewEntitySingle} from '../actions/entity.action';
import * as _ from 'lodash';
import {GetEntityOptions, ListEntityOptions} from '../../types/exchange';

export class EntityStateModel {
    [modelName: string]: {
        lists: {
            [listName: string]: EntityList,
        },
        singles: {
            [singleName: string]: EntitySingle,
        }
    }
}

@State<EntityStateModel>({
    name: 'entities',
    defaults: {
    }
})
@Injectable()
export class EntityState {
    constructor(private entityService: EntityService) {
    }

    /**
     *
     * @param ctx
     * @param action
     */
    @Action(NewEntitySingle)
    newEntitySingle(ctx: StateContext<EntityStateModel>, action: NewEntitySingle): void {
        const state = ctx.getState();
        const {id, modelName} = action;
        const newSingle = new EntitySingle(id, modelName);
        const modelSingles = state[modelName]?.singles;
        ctx.setState({
            ...state,
            [modelName]: {
                ...state[modelName],
                singles: {
                    ...modelSingles,
                    [id]: newSingle,
                }
            }
        });
        const sendAction = new SetEntitySingle(modelName, null as any); // as any used to avoid type error since we still dont have data
        const getOptions = new GetEntityOptions(modelName, id);
        this.entityService.requestAction(getOptions, sendAction);
    }

    /**
     * Action takes an entity to be evaluated and used to update any list of its type that should contain it
     * @param ctx
     * @param action
     */
    @Action(SetEntitySingle)
    setEntity(ctx: StateContext<EntityStateModel>, action: SetEntitySingle): void {
        const state = ctx.getState();
        const {modelName, data: entityData} = action;

        const modelLists = state[modelName]?.lists || {};

        let remove = _.pickBy(modelLists, (list, k) => contains(list, entityData) && !belongs(list, entityData));
        let edit = _.pickBy(modelLists, (list, k) => belongs(list, entityData));

        // Remove from those where it is present but no longer fits
        remove = _.mapValues(remove, (list) => {
            const idx = list.items.findIndex(item => item.id === entityData.id);
            const newArray = [...list.items];
            newArray.splice(idx, 1);
            return {
                ...list,
                items: newArray
            };
        });
        // Edit on those where it is present
        edit = _.mapValues(edit, (list) => combine(list, entityData));

        // Search for the same entity among singles and replace if found
        let modelSingles = state[modelName]?.singles || {};
        modelSingles = _.mapValues(modelSingles, (entity: EntitySingle) => entity.data.id === entityData.id ? {...entity, loading: false, data: entityData} : entity);

        ctx.setState({
            ...state,
            [modelName]: {
                ...state[modelName],
                lists: {
                    ...modelLists,
                    ...remove,
                    ...edit,
                },
                singles: {
                    ...modelSingles
                }
            }
        });
    }

    /**
     *
     * @param ctx
     * @param action
     */
    @Action(NewEntityList)
    newEntityList(ctx: StateContext<EntityStateModel>, action: NewEntityList): void {
        const state = ctx.getState();
        const {modelName, filters, sort, count, listName} = action;
        const listOptions = new ListEntityOptions(modelName, filters, sort, count, 0);
        const newList = new EntityList(listName, listOptions);
        const modelLists = state[modelName]?.lists;
        ctx.setState({
            ...state,
            [modelName]: {
                ...state[modelName],
                lists: {
                    ...modelLists,
                    [listName]: newList,
                }
            }
        });
        const sendAction = new SetEntityList(listName, listOptions);
        this.entityService.requestAction(listOptions, sendAction);
    }

    /**
     * Action takes entity list and places it in its place
     * @param ctx
     * @param action
     */
    @Action(SetEntityList)
    setEntityList(ctx: StateContext<EntityStateModel>, action: SetEntityList): void {
        // TODO: use action.page to push into list if necessary
        const state = ctx.getState();

        const {listOptions: {modelName}, listName} = action;
        const modelLists = state[modelName]?.lists;
        const prevList = state[modelName]?.lists[listName];

        // We could receive a list we havent requested (sockets broadcasting)
        if (!prevList) {
            return;
        }

        const items = prevList.items.slice(0, action.offset);
        items.push(...action.data);
        ctx.setState({
            ...state,
            [modelName]: {
                ...state[modelName],
                lists: {
                    ...modelLists,
                    [listName]: {
                        ...prevList,
                        loading: false,
                        items,
                    },
                }
            }
        });
    }
}
