import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {LoggedIn, Login} from '../actions/auth.action';
import {SocketPing} from '../actions/socket.action';
import {SocketsService} from '../../services/sockets/sockets.service';

export class SocketStateModel {
    broadcastGroup: any; // TODO: import UserData type from server
}

@State<SocketStateModel>({
    name: 'socket',
    defaults: {
        broadcastGroup: null,
    }
})
@Injectable()
export class AuthState {
    constructor(private socketsService: SocketsService) {
    }

    /**
     * @param ctx
     * @param action
     */
    @Action(SocketPing)
    ping(ctx: StateContext<SocketStateModel>, action: SocketPing): void {
        this.socketsService.ping();
    }
}
