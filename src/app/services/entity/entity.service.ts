import {Injectable} from '@angular/core';
import {RestApiService} from '../restapi/rest-api.service';
import {SocketsService} from '../sockets/sockets.service';
import {RemoteActionDefinition, BaseEntityOptions, RequestPayload, ResponsePayload, Sort} from '../../types/exchange';
import {Store} from '@ngxs/store';

@Injectable({
    providedIn: 'root'
})
export class EntityService {

    constructor(private restApi: RestApiService, private sockets: SocketsService, private store: Store) {
        this.sockets.onReceive(this.onReceiveFromServer.bind(this));
    }
    private onReceiveFromServer(payload: ResponsePayload): void {
        // If response contains action to perfrom with data, pass along with data
        if (payload.returnData?.action) {
            if (payload.returnData.action.success && payload.status === 'success') {
                this.store.dispatch({...payload.returnData.action.success, data: payload.data});
            }
            if (payload.returnData.action.error && payload.status === 'error') {
                this.store.dispatch({...payload.returnData.action.error, data: payload.error});
            }
        }
    }
    requestAction<EntityOptionsType extends BaseEntityOptions = BaseEntityOptions>(
        entityOptions: EntityOptionsType,
        successAction?: RemoteActionDefinition,
        errorAction?: RemoteActionDefinition,
        method: 'http' | 'sockets' = 'http'): void {
            const payload: RequestPayload<BaseEntityOptions> = new RequestPayload<BaseEntityOptions>(
                {},
                entityOptions,
                successAction || errorAction ? {
                    action: {
                        ...successAction ? {success: {...successAction, type: (successAction.constructor as any).type}} : {},
                        ...errorAction ? {error: {...errorAction, type: (errorAction.constructor as any).type }} : {},
                    },
                } : undefined,
            );
            if (method === 'http') { // TODO: check preffered method
                this.restApi.request(`/entities`, payload).then(this.onReceiveFromServer.bind(this));
            } else {
                this.sockets.send({ endpoint: `/entities`, ...payload});
            }
    }
}
