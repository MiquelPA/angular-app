import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RequestPayload, ResponsePayload} from '../../types/exchange';
import {environment} from '../../../environments/environment';
import {filter, first} from 'rxjs/operators';
import * as _ from 'lodash';
import {Select, Store} from '@ngxs/store';
import {combineLatest, Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {
    // TODO: type state
    @Select((state: any) => state.auth?.user?.token) _token$: Observable<string>;
    private token$: Observable<string>;

    constructor(private http: HttpClient, private store: Store) {
        this.token$ = this._token$.pipe(filter(t => !!t));
    }
    public async request(path: string, payload: RequestPayload<any>): Promise<ResponsePayload> {
        const authToken = await this.token$.pipe(first()).toPromise();
        // const authToken = this.store.select(state => state.auth.user.token);
        const endpoint = _.trim(path, '/ ');
        return this.http.request(
            'post',
            `${environment.apiUrl}/${endpoint}`, {
                body: payload,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                }
            }).pipe(first()).toPromise() as Promise<ResponsePayload>;
    }
}
