import {Injectable, OnDestroy} from '@angular/core';
import {io, Socket} from 'socket.io-client';
import {ResponsePayload, WebSocketRequestPayload} from '../../types/exchange';
import {Select} from '@ngxs/store';
import {combineLatest, Observable, Subject, Subscription} from 'rxjs';
import {filter, first, map, pairwise, startWith, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SocketsService implements OnDestroy {
    private socket: Socket;
    // TODO: type state
    @Select((state: any) => state.auth?.user?.token) token$: Observable<string>;
    private socket$: Observable<Socket>;
    private onReceiveCallback$ = new Subject<(response: ResponsePayload) => void>();
    private socketSubscription: Subscription;

    constructor() {
        this.socket$ = this.token$.pipe(
            filter(t => !!t),
            map((token) => io(environment.apiUrl, {
                auth: {
                    token
                }
            })),
            startWith(null as unknown as Socket),
            pairwise(),
            map(([prevSocket, socket]: [Socket, Socket]) => {
                prevSocket?.disconnect();
                return socket;
            }),
        );
        this.socketSubscription = combineLatest([
            this.socket$,
            this.onReceiveCallback$,
        ]).subscribe( ([socket, onReceiveCallback]) => {
            socket.on('response', onReceiveCallback);
        });
    }
    public async onReceive(callback: (response: ResponsePayload) => void): Promise<void> {
        this.onReceiveCallback$.next(callback);
    }
    public async send(data?: WebSocketRequestPayload<any>): Promise<void> {
        const socket = await this.socket$.pipe(first()).toPromise();
        socket.emit(
            'request',
            {
                ...data,
            },
            (response: ResponsePayload) => {
                console.log('Socket Acknowledgement', response);
            });
    }
    ngOnDestroy(): void {
        this.socketSubscription?.unsubscribe();
    }
}
