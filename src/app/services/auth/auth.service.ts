import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {first} from 'rxjs/operators';
import { ResponsePayload} from '../../types/exchange';
import {environment} from '../../../environments/environment';
import {SocketsService} from '../sockets/sockets.service';
import {Store} from '@ngxs/store';
import {LoggedIn} from '../../redux/actions/auth.action';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient, private socket: SocketsService, private store: Store) {
    }

    public login(email: string, password: string): void {
        this.httpLogin(email, password);
    }

    public async httpLogin(email: string, password: string): Promise<void> {
        const response: ResponsePayload = (await this.http.request(
            'post',
            `${environment.apiUrl}/login`, {
                body: {
                    data: {
                        email,
                        password
                    }
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).pipe(first()).toPromise()) as ResponsePayload;
        // TODO: catch unexpected response
        if (response.status === 'success') {
            this.store.dispatch(new LoggedIn(response.data));
        }
    }
}
