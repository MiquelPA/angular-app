import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {NgxsModule} from '@ngxs/store';
//import { SocketIoModule, SocketIoConfig } from 'socket.io-client';
import {environment} from '../environments/environment';
import {AuthState} from './redux/states/auth.state';
import {EntityState} from './redux/states/entity.state';

/*const socketIoConfig: SocketIoConfig = { url: environment.apiUrl, options: {
        withCredentials: true
    } };*/

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        NgxsModule.forRoot([AuthState, EntityState]),
        //SocketIoModule.forRoot(socketIoConfig),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
